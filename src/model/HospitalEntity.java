package model;

import dto.FarmacyDto;
import dto.HospitalDto;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Data
@Entity
@Table(name = "hospitals")
public class HospitalEntity extends BaseEntity<HospitalDto> {
    @NotNull
    private String name;
    @NotNull
    private AddressEntity location;
    @OneToMany
    private List<DoctorEntity> doctorEntityList;
    @OneToMany
    private List<SectorEntity> sectorEntityList;

    public HospitalEntity() {
    }


    @Override
    public HospitalDto toDto() {
        return null;
    }

    @Override
    public void fromDto(HospitalDto dto) {

    }
}
