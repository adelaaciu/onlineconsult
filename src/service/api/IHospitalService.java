package service.api;

import dto.AddressDto;
import dto.HospitalDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IHospitalService {
    public HospitalDto getHospital(Long id);

    public List<HospitalDto> getAllHospitals();

    public HospitalDto addHospital(HospitalDto dto);

    public HospitalDto updateHospital(Long id, HospitalDto dto);

    public void deleteHospital(Long id);

    public boolean canDelete(long id);

    public List<HospitalDto> getHospitalsByLocation(AddressDto address);
}
