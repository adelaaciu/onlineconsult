package rest.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import rest.api.DoctorEndpoint;
import dto.DoctorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.api.IDoctorService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
public class DoctorController implements DoctorEndpoint{
    @Autowired
    private IDoctorService doctorService;

    @Override
    public ResponseEntity<DoctorDto> getDoctor(@PathVariable("id") long id) {
        DoctorDto dto = doctorService.getDoctor(id);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity<List<DoctorDto>> getDoctorsByHospital(String name) {
        List<DoctorDto> dtos = doctorService.getDoctorsByHospital(name);

        if (dtos != null)
            return ResponseEntity.ok(dtos);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<DoctorDto>> getAllDoctors() {
        List<DoctorDto> dtos = doctorService.getAllDoctors();

        if (dtos.size() > 0)
            return ResponseEntity.ok(dtos);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity<DoctorDto> addDoctor(@RequestBody DoctorDto dto) {
        DoctorDto doctorDto = doctorService.addDoctor(dto);

        if (doctorDto == null) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @Override
    public ResponseEntity<DoctorDto> updateDoctor(@PathVariable("id") long id, @RequestBody DoctorDto dto) {
        DoctorDto doctorDto = doctorService.updateDoctor(id, dto);

        if (doctorDto != null) {
            return ResponseEntity.ok(doctorDto);
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity deleteDoctor(@PathVariable("id") long id) {
        if(doctorService.canDelete(id)){
            doctorService.deleteDoctor(id);
            return  ResponseEntity.status(HttpStatus.OK).build();
        }

        return  ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
