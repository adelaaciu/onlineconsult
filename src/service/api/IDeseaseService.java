package service.api;

import dto.DeseaseDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IDeseaseService {
    public DeseaseDto getDesease(Long id);

    public List<DeseaseDto> getAllDeseases();

    public DeseaseDto addDesease(DeseaseDto dto);

    public DeseaseDto updateDesease(Long id,DeseaseDto dto);

    public void deleteDesease(Long id);

    public boolean canDelete(Long id);

    public DeseaseDto getDeseaseByName(String deseaseName);

    public List<DeseaseDto> getDeseaseByMedicine(String medicineName);
}
