
package dto;

import lombok.Data;

import java.util.ArrayList;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class MedicineDto {
    private String name;
    private String composition;
    private ArrayList<String> adverseReactions;
    private ArrayList<DeseaseDto> deseaseDtos;
    private HospitalDto hospitalDto;

    // ce medicament e pt o anumite boali
    // findByDeseaseId

    // la ce boli sunt bune medicamentul x
    // findAllByMedicineId
    // findByMedicineId
    public MedicineDto() {
    }
}
