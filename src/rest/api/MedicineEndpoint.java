package rest.api;

import dto.MedicineDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@RestController
@RequestMapping("/medicines")
public interface MedicineEndpoint {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<MedicineDto> getMedicineById(long id);

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public ResponseEntity<MedicineDto> getMedicineByName(String name);

    @RequestMapping(value = "/desease/{name}", method = RequestMethod.GET)
    public ResponseEntity<MedicineDto> getMedicineByDesease(String name);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<MedicineDto>> getAllMedicines();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<MedicineDto> addMedicine(MedicineDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<MedicineDto> updateMedicine(long id, MedicineDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteMedicine(long id);
}
