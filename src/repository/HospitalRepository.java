package repository;

import dto.AddressDto;
import model.AddressEntity;
import model.HospitalEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Repository
public interface HospitalRepository extends PagingAndSortingRepository<HospitalEntity, Long> {
    List<HospitalEntity> getHospitalsByLocation(AddressEntity address);

    HospitalEntity getHospitalByName(String name);
}
