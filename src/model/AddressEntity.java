package model;

import dto.AddressDto;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Adela on 14.03.2017.
 */
@Data
@Entity
@Table(name = "location")
public class AddressEntity extends BaseEntity<AddressDto>{

    String city;
    String country;
    String adress;

    public AddressEntity() {
    }

    @Override
    public AddressDto toDto() {
        return null;
    }

    @Override
    public void fromDto(AddressDto dto) {

    }


}
