package service.impl;

import dto.DeseaseDto;
import dto.DoctorDto;
import model.DoctorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DoctorRepository;
import service.api.IDoctorService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class DoctorService implements IDoctorService {
    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public DoctorDto getDoctor(Long id) {
        DoctorEntity doctorEntity = doctorRepository.findOne(id);

        if (doctorEntity != null) {
            return doctorEntity.toDto();
        }
        return null;
    }

    @Override
    public List<DoctorDto> getDoctorsByHospital(String name) {
        List<DoctorEntity> doctors = doctorRepository.getDoctorsByHospital(name);

        if (doctors != null) {
            return convertEntitisToDtos(doctors);
        }
        return null;
    }

    @Override
    public List<DoctorDto> getAllDoctors() {
        List<DoctorEntity> doctors = (List<DoctorEntity>) doctorRepository.findAll();
        List<DoctorDto> dtos = convertEntitisToDtos(doctors);

        if (dtos.size() > 0)
            return dtos;

        return null;
    }

    private List<DoctorDto> convertEntitisToDtos(List<DoctorEntity> doctors) {
        List<DoctorDto> dtos = new ArrayList<>();

        doctors.forEach(x -> {
            dtos.add(x.toDto());
        });

        return dtos;
    }


    //DoctorEntity??

    @Override
    public DoctorDto addDoctor(DoctorDto dto) {

        if (doctorRepository.findByName(dto.getName()) != null) {
            return null;
        }

        DoctorEntity entity = new DoctorEntity();
        entity.fromDto(dto);
        entity = doctorRepository.save(entity);

        return entity.toDto();
    }

    @Override
    public DoctorDto updateDoctor(Long id, DoctorDto dto) {
        if (doctorRepository.exists(id)) {
            DoctorEntity entity = doctorRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            return entity.toDto();
        }
        return null;
    }

    @Override
    public void deleteDoctor(Long id) {
        doctorRepository.delete(id);
    }

    @Override
    public boolean canDelete(Long id) {
        if (doctorRepository.exists(id))
            return true;
        return false;
    }


}
