package service.api;

import dto.AddressDto;
import dto.FarmacyDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@Service
public interface IFarmacyService {

    public FarmacyDto getFarmacy(Long id);

    public List<FarmacyDto> getAllFarmacys();

    public FarmacyDto addFarmacy(FarmacyDto dto);

    public FarmacyDto updateFarmacy(Long id, FarmacyDto dto);

    public void deleteFarmacy(Long id);

    public FarmacyDto getFarmacyByLocation(AddressDto address);

    public boolean canDelete(long id);
}
