package model;

import dto.DoctorDto;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by Adela on 07.03.2017.
 */
@Data
@Entity
@Table(name = "doctors")
public class DoctorEntity extends BaseEntity<DoctorDto> {
    @NotNull
    private String name;
    private String phoneNo;
    private String email;
    private AddressEntity adress;
    @NotNull
    private SectorEntity sectorEntity;
    private String startHour;
    private String endHour;
    private HospitalEntity hospitalEntity;

    public DoctorEntity() {
    }

    @Override
    public DoctorDto toDto() {
        return null;
    }

    @Override
    public void fromDto(DoctorDto dto) {

    }
}
