package rest.impl;

import dto.AddressDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import rest.api.HospitalEndpoint;
import dto.HospitalDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.api.IHospitalService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
public class HospitalController implements HospitalEndpoint {

    @Autowired
    IHospitalService hospitalService;

    @Override
    public ResponseEntity<HospitalDto> getHospital(@PathVariable("id") long id) {
        HospitalDto dto = hospitalService.getHospital(id);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<HospitalDto>> getHospitalsByLocation(@RequestBody AddressDto address) {
        List<HospitalDto> dtos = hospitalService.getHospitalsByLocation(address);

        if (dtos != null) {
            return ResponseEntity.ok(dtos);
        } else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<HospitalDto>> getAllHospitals() {
        List<HospitalDto> dtos = hospitalService.getAllHospitals();

        if (dtos.size() > 0)
            return ResponseEntity.ok(dtos);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity<HospitalDto> addHospital(@RequestBody HospitalDto dto) {
        HospitalDto hospitalDto = hospitalService.addHospital(dto);

        if (hospitalDto == null) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);

    }

    @Override
    public ResponseEntity<HospitalDto> updateHospital(@PathVariable("id") long id, @RequestBody HospitalDto dto) {
        HospitalDto hospitalDto = hospitalService.updateHospital(id, dto);

        if (hospitalDto != null) {
            return ResponseEntity.ok(hospitalDto);
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity deleteHospital(@PathVariable("id") long id) {
        if (hospitalService.canDelete(id)) {
            hospitalService.deleteHospital(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();


    }
}
