package rest.api;

import dto.DoctorDto;

import dto.DoctorDto;
import model.DoctorEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@RestController
@RequestMapping("/doctors")
public interface DoctorEndpoint {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<DoctorDto> getDoctor(long id);

    @RequestMapping(value = "/hospital/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<DoctorDto>> getDoctorsByHospital(String name);

    //dupa spital si sector??

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<DoctorDto>> getAllDoctors();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<DoctorDto> addDoctor( DoctorDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<DoctorDto> updateDoctor(long id, DoctorDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteDoctor( long id);
}
