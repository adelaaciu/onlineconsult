package rest.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import rest.api.DeseaseEndpoint;
import dto.DeseaseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.api.IDeseaseService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
public class DeseaseController implements DeseaseEndpoint {
    @Autowired
    private IDeseaseService deseaseService;

    @Override
    public ResponseEntity<DeseaseDto> getDeseaseById(@PathVariable("id") long id) {
        DeseaseDto dto = deseaseService.getDesease(id);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<DeseaseDto> getDeseaseByName(@PathVariable("deseaseName") String deseaseName) {
        DeseaseDto dto = deseaseService.getDeseaseByName(deseaseName);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<DeseaseDto>> getDeseaseByMedicine(@PathVariable("medicineName") String medicineName) {
        List<DeseaseDto> dtos = deseaseService.getDeseaseByMedicine(medicineName);

        if (dtos != null)
            return ResponseEntity.ok(dtos);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<DeseaseDto>> getAllDeseases() {
        List<DeseaseDto> dtos = deseaseService.getAllDeseases();

        if (dtos.size() > 0)
            return ResponseEntity.ok(dtos);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<DeseaseDto> addDesease(@RequestBody DeseaseDto dto) {
        DeseaseDto deseaseDto = deseaseService.addDesease(dto);

        if (deseaseDto == null) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @Override
    public ResponseEntity<DeseaseDto> updateDesease(@PathVariable("id") long id, @RequestBody DeseaseDto dto) {
        DeseaseDto deseaseDto = deseaseService.updateDesease(id, dto);

        if (deseaseDto != null) {
            return ResponseEntity.ok(deseaseDto);
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity deleteDesease(@PathVariable("id") long id) {
        if(deseaseService.canDelete(id)){
            deseaseService.deleteDesease(id);
            return  ResponseEntity.status(HttpStatus.OK).build();
        }

        return  ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
