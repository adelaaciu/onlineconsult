package rest.api;

import dto.SectorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@RestController
@RequestMapping("/sectors")
public interface SectorEndpoint {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<SectorDto> getSector( long id);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<SectorDto>> getAllSectors();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<SectorDto> addSector( SectorDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<SectorDto> updateSector( long id,  SectorDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteSector( long id);
    
}
