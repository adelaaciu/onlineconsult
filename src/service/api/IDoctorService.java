package service.api;

import dto.DoctorDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IDoctorService {
    public DoctorDto getDoctor(Long id);

    public List<DoctorDto> getAllDoctors();

    public DoctorDto addDoctor(DoctorDto dto);

    public DoctorDto updateDoctor(Long id,DoctorDto dto);

    public void deleteDoctor(Long id);

    public  boolean canDelete(Long id);

    public List<DoctorDto> getDoctorsByHospital(String name);
}
