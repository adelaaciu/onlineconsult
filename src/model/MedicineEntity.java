package model;

import dto.MedicineDto;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * Created by Adela on 07.03.2017.
 */
@Data
@Entity
@Table(name = "medicines")
public class MedicineEntity extends BaseEntity<MedicineDto> {
    @NotNull
    private String name;
    private String composition;
    private ArrayList<String> adverseReactions;
    @OneToMany
    private ArrayList<DeseaseEntity> deseaseEntities;

    public MedicineEntity() {
    }

    @Override
    public MedicineDto toDto() {
        return null;
    }

    @Override
    public void fromDto(MedicineDto dto) {

    }
}
