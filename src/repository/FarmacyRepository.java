package repository;

import dto.AddressDto;
import model.AddressEntity;
import model.FarmacyEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Adela on 14.03.2017.
 */
@Repository
public interface FarmacyRepository extends PagingAndSortingRepository<FarmacyEntity,Long> {
    FarmacyEntity getFarmacyByLoacation(AddressEntity address);
}
