package service.impl;

import dto.SectorDto;
import model.HospitalEntity;
import model.SectorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.SectorRepository;
import service.api.ISectorService;
import sun.dc.pr.PRError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class SectorService implements ISectorService{
    @Autowired
    private SectorRepository sectorRepository;
    
    @Override
    public SectorDto getSector(Long id) {
        if (sectorRepository.exists(id)) {
            return sectorRepository.findOne(id).toDto();
        }
        

        return null;
    }

    @Override
    public List<SectorDto> getAllSectors() {
        List<SectorEntity> eitities = (List<SectorEntity>) sectorRepository.findAll();

        if (eitities.size() != 0)
            return convertEntitisToDtos(eitities);
        return null;
    }

    @Override
    public SectorDto addSector(SectorDto dto) {
        SectorEntity entity = new SectorEntity();
        entity.toDto();

        if (sectorRepository.getSectorByName(dto.getName()) == null) {
            sectorRepository.save(entity);
            return entity.toDto();
        }
        return null;
    }

    @Override
    public SectorDto updateSector(Long id, SectorDto dto) {

        if (sectorRepository.exists(id)) {
            SectorEntity entity = sectorRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            sectorRepository.save(entity);

            return entity.toDto();
        }

        return null;
    }

    @Override
    public void deleteSector(Long id) {
        sectorRepository.delete(id);
    }

    @Override
    public boolean canDelete(long id) {
        if (sectorRepository.exists(id))
            return true;
        return false;
    }

    private List<SectorDto> convertEntitisToDtos(List<SectorEntity> entities) {
        List<SectorDto> dtos = new ArrayList<>();

        entities.forEach(x -> {
            dtos.add(x.toDto());
        });
        return dtos;
    }
}
