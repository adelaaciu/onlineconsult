package dto;

import lombok.Data;

/**
 * Created by Adela on 14.03.2017.
 */
@Data
public class AddressDto {
    private String city;
    private String country;
    private String adress;

    public AddressDto() {
    }
}
