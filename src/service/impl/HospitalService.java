package service.impl;

import dto.AddressDto;
import dto.HospitalDto;
import model.AddressEntity;
import model.HospitalEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.HospitalRepository;
import service.api.IHospitalService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class HospitalService implements IHospitalService {
    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public HospitalDto getHospital(Long id) {
        if (hospitalRepository.exists(id)) {
            HospitalEntity entity = hospitalRepository.findOne(id);
            return entity.toDto();
        }
        return null;
    }

    @Override
    public List<HospitalDto> getHospitalsByLocation(AddressDto address) {
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.fromDto(address);
        List<HospitalEntity> entities = hospitalRepository.getHospitalsByLocation(addressEntity);

        if (entities != null) {
            List<HospitalDto> dtos = convertEntityToDto(entities);
            return dtos;
        }
        return null;
    }


    @Override
    public List<HospitalDto> getAllHospitals() {
        List<HospitalEntity> entities = (List<HospitalEntity>) hospitalRepository.findAll();

        if (entities.size() > 0) {
            return convertEntityToDto(entities);
        }
        return null;
    }

    private List<HospitalDto> convertEntityToDto(List<HospitalEntity> entities) {
        List<HospitalDto> dtos = new ArrayList<>();

        entities.forEach(x -> {
            dtos.add(x.toDto());
        });
        return dtos;
    }


    @Override
    public HospitalDto addHospital(HospitalDto dto) {
        HospitalEntity entity = new HospitalEntity();
        entity.toDto();

        if (hospitalRepository.getHospitalsByLocation(entity.getLocation()).size() > 0 && hospitalRepository.getHospitalByName(dto.getName()) == null) {
            hospitalRepository.save(entity);
            return entity.toDto();
        }
        return null;
    }

    @Override
    public HospitalDto updateHospital(Long id, HospitalDto dto) {
        if (hospitalRepository.exists(id)) {
            HospitalEntity entity = hospitalRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            hospitalRepository.save(entity);

            return entity.toDto();
        }

        return null;
    }

    @Override
    public void deleteHospital(Long id) {
        hospitalRepository.delete(id);

    }

    @Override
    public boolean canDelete(long id) {
        if (hospitalRepository.exists(id))
            return true;
        return false;
    }
}
