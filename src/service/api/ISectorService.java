package service.api;

import dto.SectorDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface ISectorService {
    public SectorDto getSector(Long id);

    public List<SectorDto> getAllSectors();

    public SectorDto addSector(SectorDto dto);

    public SectorDto updateSector(Long id, SectorDto dto);

    public void deleteSector(Long id);

    public boolean canDelete(long id);
}
