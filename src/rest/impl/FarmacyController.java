package rest.impl;

import dto.AddressDto;
import dto.FarmacyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import rest.api.FarmacyEndpoint;
import service.api.IFarmacyService;

import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@RestController
public class FarmacyController implements FarmacyEndpoint {

    @Autowired
    private IFarmacyService farmacyService;

    @Override
    public ResponseEntity<FarmacyDto> getFarmacy(@PathVariable("id") long id) {
        FarmacyDto dto = farmacyService.getFarmacy(id);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<FarmacyDto> getFarmacyByLocation(@RequestBody AddressDto address) {
        FarmacyDto dto = farmacyService.getFarmacyByLocation(address);

        if (dto != null)
            return ResponseEntity.ok(dto);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<FarmacyDto>> getAllFarmacys() {
        List<FarmacyDto> dtos = farmacyService.getAllFarmacys();

        if (dtos != null)
            return ResponseEntity.ok(dtos);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<FarmacyDto> addFarmacy(@RequestBody FarmacyDto dto) {
        FarmacyDto farmacyDto = farmacyService.addFarmacy(dto);

        if (farmacyDto == null) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @Override
    public ResponseEntity<FarmacyDto> updateFarmacy(@PathVariable("id") long id, @RequestBody FarmacyDto dto) {
        FarmacyDto farmacyDto = farmacyService.updateFarmacy(id,dto);

        if (farmacyDto == null) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);

    }

    @Override
    public ResponseEntity deleteFarmacy(@PathVariable("id") long id) {

        if(farmacyService.canDelete(id)){
            farmacyService.deleteFarmacy(id);
            return  ResponseEntity.status(HttpStatus.OK).build();
        }

        return  ResponseEntity.status(HttpStatus.NOT_FOUND).build();



    }
}
