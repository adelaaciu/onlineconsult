package service.impl;

import dto.MedicineDto;
import model.MedicineEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.MedicineRepository;
import service.api.IMedicineService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class MedicineService implements IMedicineService{
    @Autowired
    private MedicineRepository medicineRepository;

    @Override
    public MedicineDto getMedicine(Long id) {
        if (medicineRepository.exists(id)) {
            return medicineRepository.findOne(id).toDto();
        }

        return null;
    }

    @Override
    public MedicineDto getMedicineByName(String name) {
        MedicineEntity entity = medicineRepository.getMedicineByName(name);

        if (entity != null) {
            return entity.toDto();
        }

        return null;
    }

    @Override
    public MedicineDto getMedicineByDesease(String name) {
        MedicineEntity entity = medicineRepository.getMedicineByDesease(name);

        if (entity != null) {
            return entity.toDto();
        }

        return null;
    }

    @Override
    public List<MedicineDto> getAllMedicines() {
        List<MedicineEntity> eitities = (List<MedicineEntity>) medicineRepository.findAll();

        if (eitities.size() != 0)
            return convertEntitisToDtos(eitities);
        return null;
    }

    @Override
    public MedicineDto addMedicine(MedicineDto dto) {
        MedicineEntity entity = new MedicineEntity();
        entity.toDto();

        if (medicineRepository.getMedicineByName(dto.getName()) == null) {
            medicineRepository.save(entity);
            return entity.toDto();
        }
        return null;

    }

    @Override
    public MedicineDto updateMedicine(Long id, MedicineDto dto) {
        if (medicineRepository.exists(id)) {
            MedicineEntity entity = medicineRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);
            medicineRepository.save(entity);

            return entity.toDto();
        }

        return null;
    }

    @Override
    public void deleteMedicine(Long id) {
        medicineRepository.delete(id);
    }

    @Override
    public boolean canDelete(long id) {
        if (medicineRepository.exists(id))
            return true;
        return false;
    }

    private List<MedicineDto> convertEntitisToDtos(List<MedicineEntity> entities) {
        List<MedicineDto> dtos = new ArrayList<>();

        entities.forEach(x -> {
            dtos.add(x.toDto());
        });
        return dtos;
    }
}
