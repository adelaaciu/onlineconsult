package rest.api;

import dto.AddressDto;
import dto.HospitalDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@RestController
@RequestMapping("/hospitals")
public interface HospitalEndpoint {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<HospitalDto> getHospital( long id);

    @RequestMapping(value = "/location", method = RequestMethod.GET)
    public ResponseEntity<List<HospitalDto>> getHospitalsByLocation(AddressDto address);


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<HospitalDto>> getAllHospitals();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<HospitalDto> addHospital( HospitalDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<HospitalDto> updateHospital( long id,  HospitalDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteHospital( long id);
}
