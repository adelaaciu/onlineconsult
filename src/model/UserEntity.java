package model;

import dto.UserDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class UserEntity extends BaseEntity<UserDto>{

    @NotNull(message = "User can't be null")
    private String userName;
    @NotNull(message = "Password can't be null")
    private String password;
    private String name;
    private String email;
    private AddressEntity address;
    private String role;

    public UserEntity() {
    }

    @Override
    public UserDto toDto() {
        return null;
    }

    @Override
    public void fromDto(UserDto dto) {

    }
}
