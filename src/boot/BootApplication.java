package boot;

import model.DeseaseEntity;
import model.MedicineEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import repository.DeseaseRepository;

import java.util.ArrayList;
import java.util.List;

@EnableJpaRepositories
@EntityScan
@ComponentScan
@SpringBootApplication
//@PropertySource({"classpath:application.properties"})
public class BootApplication {
    private static final Logger log = LoggerFactory.getLogger(BootApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }

//    @Bean
//    public CommandLineRunner demo(DeseaseRepository deseaseRepository) {
//        return (args) -> {
//            List<String> symptomEntityList = new ArrayList<>();
//            List<MedicineEntity> medicineEntityList = new ArrayList<>();
//
//            DeseaseEntity d = new DeseaseEntity("Name", symptomEntityList, medicineEntityList);
//            deseaseRepository.save(d);
//
//            log.info("Customer SAVED:");
//        };
//    }
}
