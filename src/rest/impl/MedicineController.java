package rest.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import rest.api.MedicineEndpoint;
import dto.MedicineDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import service.api.IMedicineService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
public class MedicineController implements MedicineEndpoint {
    @Autowired
    private IMedicineService medicineService;

    @Override
    public ResponseEntity<MedicineDto> getMedicineById(@PathVariable("id") long id) {
        MedicineDto dto = medicineService.getMedicine(id);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<MedicineDto> getMedicineByName(@PathVariable("name") String name) {
        MedicineDto dto = medicineService.getMedicineByName(name);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<MedicineDto> getMedicineByDesease(@PathVariable("name") String name) {
        MedicineDto dto = medicineService.getMedicineByDesease(name);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<MedicineDto>> getAllMedicines() {
        List<MedicineDto> dtos = medicineService.getAllMedicines();

        if (dtos.size() > 0)
            return ResponseEntity.ok(dtos);
        else
            return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<MedicineDto> addMedicine(@RequestBody MedicineDto dto) {
        MedicineDto medicineDto = medicineService.addMedicine(dto);

        if (medicineDto == null) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @Override
    public ResponseEntity<MedicineDto> updateMedicine(@PathVariable("id") long id, @RequestBody MedicineDto dto) {
        MedicineDto medicineDto = medicineService.updateMedicine(id, dto);

        if (medicineDto != null) {
            return ResponseEntity.ok(medicineDto);
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity deleteMedicine(@PathVariable("id") long id) {
        if (medicineService.canDelete(id)) {
            medicineService.deleteMedicine(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
