package model;

import dto.FarmacyDto;
import lombok.Data;

/**
 * Created by Adela on 14.03.2017.
 */
@Data
public class FarmacyEntity extends BaseEntity<FarmacyDto> {
    private String name;
    private String startHour;
    private String endHour;
    private AddressEntity location;

    public FarmacyEntity() {
    }

    @Override
    public FarmacyDto toDto() {
        return null;
    }

    @Override
    public void fromDto(FarmacyDto dto) {

    }
}
