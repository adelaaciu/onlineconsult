package rest.api;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Adela on 12.03.2017.
 */
@RestController
@RequestMapping("/users")
public interface UserEndpoint {
}
