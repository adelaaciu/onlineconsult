package repository;

import model.AddressEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Adela on 14.03.2017.
 */
@Repository
public interface AddressRepository extends PagingAndSortingRepository<AddressEntity,Long>{
}
