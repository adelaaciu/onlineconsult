package service.impl;

import dto.DeseaseDto;
import model.DeseaseEntity;
import model.MedicineEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DeseaseRepository;
import service.api.IDeseaseService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@Service
public class DeseaseService implements IDeseaseService {
    @Autowired
    private DeseaseRepository deseaseRepository;

    @Override
    public DeseaseDto getDesease(Long id) {
        DeseaseEntity deseaseEntity = deseaseRepository.findOne(id);

        if (deseaseEntity != null) {
            return deseaseEntity.toDto();
        }

        return null;
    }

    @Override
    public DeseaseDto getDeseaseByName(String deseaseName) {
        DeseaseEntity deseaseEntity = deseaseRepository.findByName(deseaseName);

        if (deseaseEntity != null) {
            return deseaseEntity.toDto();
        }

        return null;
    }

    @Override
    public List<DeseaseDto> getDeseaseByMedicine(String medicineName) {
        List<DeseaseEntity> deseaseEntity = deseaseRepository.findByMedicine(medicineName);
        List<DeseaseDto> dtos = convertEntityListToDtoList(deseaseEntity);

        return dtos;
    }

    @Override
    public List<DeseaseDto> getAllDeseases() {
        List<DeseaseEntity> deseaseEntityList = (List<DeseaseEntity>) deseaseRepository.findAll();
        List<DeseaseDto> dtos = convertEntityListToDtoList(deseaseEntityList);

        return dtos;
    }

    private List<DeseaseDto> convertEntityListToDtoList(List<DeseaseEntity> deseaseEntityList) {
        List<DeseaseDto> dtos;

        if (deseaseEntityList.size() != 0) {
            dtos = new ArrayList<>();

            deseaseEntityList.forEach(x -> {
                dtos.add(x.toDto());
            });

            return dtos;
        }
        return null;
    }

    @Override
    public DeseaseDto addDesease(DeseaseDto dto) {
        if (deseaseRepository.findByName(dto.getName()) != null) {
            return null;
        }

        DeseaseEntity entity = new DeseaseEntity();
        entity.fromDto(dto);
        entity = deseaseRepository.save(entity);

        return entity.toDto();
    }

    @Override
    public DeseaseDto updateDesease(Long id, DeseaseDto dto) {
        DeseaseEntity entity = deseaseRepository.findOne(id);

        if (entity != null) {
            List<MedicineEntity> medicineEntityList = getMedicineEntityList(dto);
            entity.setMedicineEntityList(medicineEntityList);
            entity.setName(dto.getName());
            entity.setSymptomEntityList(dto.getSymptomList());
            entity.setId(id);
            deseaseRepository.save(entity);

            return dto;
        }

        return null;
    }

    private List<MedicineEntity> getMedicineEntityList(DeseaseDto dto) {
        List<MedicineEntity> medicineEntityList = new ArrayList<>();

        dto.getMedicineList().forEach(x -> {

            MedicineEntity medicineEntity = new MedicineEntity();
            medicineEntity.fromDto(x);
            medicineEntityList.add(medicineEntity);

        });
        return medicineEntityList;
    }

    @Override
    public void deleteDesease(Long id) {
        DeseaseEntity entity = deseaseRepository.findOne(id);
        deseaseRepository.delete(entity);
    }

    @Override
    public boolean canDelete(Long id) {
        if (deseaseRepository.exists(id))
            return true;
        return false;
    }

}
