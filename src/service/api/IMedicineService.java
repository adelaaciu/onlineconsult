package service.api;

import dto.MedicineDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Service
public interface IMedicineService {
    public MedicineDto getMedicine(Long id);

    public List<MedicineDto> getAllMedicines();

    public MedicineDto addMedicine(MedicineDto dto);

    public MedicineDto updateMedicine(Long id, MedicineDto dto);

    public void deleteMedicine(Long id);

    public MedicineDto getMedicineByName(String name);

    public MedicineDto getMedicineByDesease(String name);

    public boolean canDelete(long id);
}
