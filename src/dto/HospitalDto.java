package dto;

import lombok.Data;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class HospitalDto {
    private String name;
    private AddressDto location;
    private List<DoctorDto> doctorList;
    private List<SectorDto> sectorList;

    public HospitalDto() {
    }

}
