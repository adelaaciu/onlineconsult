package model;

import dto.SectorDto;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by Adela on 07.03.2017.
 */
@Data
@Entity
@Table(name = "sectors")
public class SectorEntity extends BaseEntity<SectorDto>{
    @NotNull
    private String name;
    private String level;
    private int chambers;
    public static int NO_MAX_OF_PERSONS = 50;
    @ManyToOne
    private HospitalEntity hospital;

    public SectorEntity() {
    }

    @Override
    public SectorDto toDto() {
        return null;
    }

    @Override
    public void fromDto(SectorDto dto) {

    }
}
