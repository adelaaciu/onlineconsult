package rest.api;

import dto.DeseaseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@RestController
@RequestMapping("/deaseses")
public interface DeseaseEndpoint {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<DeseaseDto> getDeseaseById(long id);

    @RequestMapping(value = "/{deseaseName}", method = RequestMethod.GET)
    public ResponseEntity<DeseaseDto> getDeseaseByName(String deseaseName);

    @RequestMapping(value = "/medicine/{medicineName}", method = RequestMethod.GET)
    public ResponseEntity<List<DeseaseDto>> getDeseaseByMedicine(String medicineName);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<DeseaseDto>> getAllDeseases();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<DeseaseDto> addDesease(DeseaseDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<DeseaseDto> updateDesease(long id, DeseaseDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteDesease(long id);
}
