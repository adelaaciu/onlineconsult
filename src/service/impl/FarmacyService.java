package service.impl;

import dto.AddressDto;
import dto.FarmacyDto;
import model.AddressEntity;
import model.FarmacyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.FarmacyRepository;
import service.api.IFarmacyService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@Service
public class FarmacyService implements IFarmacyService {
    @Autowired
    private FarmacyRepository farmacyRepository;

    @Override
    public FarmacyDto getFarmacy(Long id) {

        if (farmacyRepository.exists(id)) {
            return farmacyRepository.findOne(id).toDto();
        }

        return null;
    }

    @Override
    public FarmacyDto getFarmacyByLocation(AddressDto address) {
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.fromDto(address);
        FarmacyEntity farmacyEntity = farmacyRepository.getFarmacyByLoacation(addressEntity);

        if (farmacyEntity != null) {
            return farmacyEntity.toDto();
        }
        return null;
    }


    @Override
    public List<FarmacyDto> getAllFarmacys() {
        List<FarmacyEntity> eitities = (List<FarmacyEntity>) farmacyRepository.findAll();

        if (eitities.size() != 0)
            return convertEntitisToDtos(eitities);
        return null;
    }

    private List<FarmacyDto> convertEntitisToDtos(List<FarmacyEntity> eitities) {
        List<FarmacyDto> dtos = new ArrayList<>();
        eitities.forEach(x -> {
            dtos.add(x.toDto());
        });

        return dtos;
    }

    @Override
    public FarmacyDto addFarmacy(FarmacyDto dto) {
        FarmacyEntity entity = new FarmacyEntity();
        entity.toDto();

        if (farmacyRepository.getFarmacyByLoacation(entity.getLocation()) == null) {
            farmacyRepository.save(entity);
            return entity.toDto();
        }
        return null;
    }

    @Override
    public FarmacyDto updateFarmacy(Long id, FarmacyDto dto) {
        if (farmacyRepository.exists(id)) {
            FarmacyEntity entity = farmacyRepository.findOne(id);
            entity.fromDto(dto);
            entity.setId(id);

            return entity.toDto();
        }

        return null;
    }

    @Override
    public void deleteFarmacy(Long id) {
        farmacyRepository.delete(id);
    }

    @Override
    public boolean canDelete(long id) {
        if(farmacyRepository.exists(id))
            return true;
        return false;
    }
}
