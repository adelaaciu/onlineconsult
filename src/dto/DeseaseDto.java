package dto;

import lombok.Data;

import java.util.List;

/**
 * Created by Adela on 12.03.2017.
 */
@Data
public class DeseaseDto {
    private String name;
    private List<String> symptomList;
    private List<MedicineDto> medicineList;

    public DeseaseDto() {
    }
}
