package model;

import dto.DeseaseDto;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */

@Data
@Entity
@Table(name = "deseases")
public class DeseaseEntity extends BaseEntity<DeseaseDto> {
//    @NotNull(message = "Deseasentity is null")
    private String name;
    private List<String> symptomEntityList;
//    @OneToMany
    private List<MedicineEntity> medicineEntityList;

    public DeseaseEntity(String name, List<String> symptomEntityList, List<MedicineEntity> medicineEntityList) {
        this.name = name;
        this.symptomEntityList = symptomEntityList;
        this.medicineEntityList = medicineEntityList;
    }

    public DeseaseEntity() {
    }

    @Override
    public DeseaseDto toDto() {
        return null;
    }

    @Override
    public void fromDto(DeseaseDto dto) {

    }
}
