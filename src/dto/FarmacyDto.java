package dto;

import lombok.Data;
import model.AddressEntity;

/**
 * Created by Adela on 14.03.2017.
 */
@Data
public class FarmacyDto {
    private String name;
    private String startHour;
    private String endHour;
    private AddressDto location;

    public FarmacyDto() {
    }
}
