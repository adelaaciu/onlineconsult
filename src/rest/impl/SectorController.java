package rest.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import rest.api.SectorEndpoint;
import dto.SectorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.api.IDeseaseService;
import service.api.ISectorService;

import java.util.List;

/**
 * Created by Adela on 07.03.2017.
 */
@RestController
public class SectorController implements SectorEndpoint {
    @Autowired
    private ISectorService sectorService;

    @Override
    public ResponseEntity<SectorDto> getSector(@PathVariable("id") long id) {
        SectorDto dto = sectorService.getSector(id);
        if (dto != null) {
            return ResponseEntity.ok(dto);
        } else return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<List<SectorDto>> getAllSectors() {
        List<SectorDto> dtos = sectorService.getAllSectors();
        if (dtos != null) {
            return ResponseEntity.ok(dtos);
        } else return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @Override
    public ResponseEntity<SectorDto> addSector(@RequestBody  SectorDto dto) {

        SectorDto sectorDto = sectorService.addSector(dto);

        if (sectorDto == null) {
            return (ResponseEntity) ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @Override
    public ResponseEntity<SectorDto> updateSector(@PathVariable("id") long id, @RequestBody SectorDto dto) {

        SectorDto sectorDto = sectorService.updateSector(id, dto);

        if (sectorDto != null) {
            return ResponseEntity.ok(sectorDto);
        }

        return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity deleteSector(@PathVariable("id") long id) {
        if (sectorService.canDelete(id)) {
            sectorService.deleteSector(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }
}
