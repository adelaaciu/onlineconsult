package repository;

import model.MedicineEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Adela on 07.03.2017.
 */
@Repository
public interface MedicineRepository extends PagingAndSortingRepository<MedicineEntity, Long> {
    MedicineEntity getMedicineByName(String name);

    MedicineEntity getMedicineByDesease(String name);
}
