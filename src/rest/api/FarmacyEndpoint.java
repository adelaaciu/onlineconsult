package rest.api;

import dto.AddressDto;
import dto.FarmacyDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Adela on 14.03.2017.
 */
@RestController
@RequestMapping("/farmacies")
public interface FarmacyEndpoint {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<FarmacyDto> getFarmacy(long id);

    @RequestMapping(value = "/location", method = RequestMethod.GET)
    public ResponseEntity<FarmacyDto> getFarmacyByLocation(AddressDto address);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<FarmacyDto>> getAllFarmacys();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<FarmacyDto> addFarmacy(FarmacyDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<FarmacyDto> updateFarmacy(long id, FarmacyDto dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteFarmacy(long id);
}
